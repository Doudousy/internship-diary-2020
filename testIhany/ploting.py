import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# import tkinter as tk

s = pd.Series([1, 3, 5, np.nan, 6, 8])
print(s)

#plt.close('all')

ts = pd.Series(np.random.randn(1000), index=pd.date_range('1/1/2000', periods=1000))
ts = ts.cumsum()
ts.plot()
plt.show(block=True)
plt.interactive(False)