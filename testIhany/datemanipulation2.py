import numpy as np
import pandas as pd
import datetime
from pandas.tseries.offsets import DateOffset
dti = pd.to_datetime(['1/1/2018', np.datetime64('2018-01-01'),datetime.datetime(2018, 1, 1)])
print(dti)
print(dti[0])
ts = pd.Timestamp('2017-01-01 09:10:11')

ts1=ts + DateOffset(months=3)
ts1=ts + DateOffset(months=3,days=1)

ts = pd.Timestamp('2017-01-01 09:10:11')
ts2=ts + DateOffset(years=1,months=2,days=3,hours=4,minutes=5.5,seconds=6.006,microseconds=7,nanoseconds=8)

print(ts)
print(ts1)
print(ts2)

d = dict({'price': [10, 11, 9, 13, 14, 18, 17, 19],
          'volume': [50, 60, 40, 100, 50, 100, 40, 50]})
df = pd.DataFrame(d)
#generation de succession de date
df['week_starting'] = pd.date_range('01/01/2018',periods=8,freq='W')
print(df)
print(df.resample('M', on='week_starting').mean())

