import tkinter as tk
from journal import Act, pd
class Application(tk.Frame):
    def __init__(self, master = None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.greeting = tk.Label(self, text = "JOURNAL")
        self.greeting.pack(side = "top")

        self.themeLabel = tk.Label(self, text = "Theme")
        self.themeLabel.pack(side = "top")

        self.entryTheme = tk.Entry(self, fg = "black", bg = "white", width = 50)
        self.entryTheme.pack(side = "top")

        self.descriptionLabel = tk.Label(self, text = "Description")
        self.descriptionLabel.pack(side = "top")

        self.entryDescritpion = tk.Text(self, fg = "black", bg = "white", width = 100, height = 25)
        self.entryDescritpion.pack(side = "top")

        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "AJOUT AU JOURNAL:"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side = "top")

        self.quit = tk.Button(self, text = "QUIT", fg = "red",
                                command = self.master.destroy)
        self.quit.pack(side = "bottom")

    def say_hi(self):
        print("hi there, everyone!")
        actVaovao = Act(1, self.entryTheme.get(), self.entryDescritpion.get("1.0", 'end-1c'))
        datas_journal = pd.read_csv("testIhany/datas/diary2.csv", sep = ";")
        datas_journal = datas_journal.set_index("id")
        datas_journal = actVaovao.ajout(datas_journal)
        datas_journal.to_csv("testIhany/datas/diary2.csv", sep = ";")
        self.entryTheme.delete(0, 'end')
        self.entryDescritpion.delete('1.0', 'end')

root = tk.Tk()
app = Application(master = root)
app.mainloop()
