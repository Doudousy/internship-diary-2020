import pandas as pd
from pandas.testing import assert_frame_equal

df1 = pd.DataFrame({'a': [1, 2], 'b': [3, 4]})

df2 = pd.DataFrame({'a': [1, 2], 'b': [3.0, 4.0]})
try:
    assert_frame_equal(df1, df2)
except AssertionError as error_assert:
    print(error_assert)#error message
print(df1)
print()
print(df1.to_json())
print()
print(df1.to_json(orient='values'))
print()
print(df1.to_json(orient='table'))
print()
print(df1.to_json(orient='split'))
print()
print(df1.to_json(orient='index'))
print()
print(df1.to_json(orient='records'))
print()
print(df1.values)
print(df1.values.tolist())
print()
print(df1.to_dict())
print(df1.to_dict()['a'])
print()
print(df1.to_dict('records'))
print(df1.to_dict('records')[0])
print()
print(df1.T.to_dict())
print(df1.T.to_dict()[0])
    
