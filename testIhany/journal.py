from datetime import datetime
import pandas as pd



class Act:
    def __init__(self, id, theme, description, date = datetime.today().strftime('%Y-%m-%d')):
        self.id = id
        self.theme = theme
        self.description = (''+description).replace('\n', '\t')
        self.date = date

    def act_object_to_data_frame(self):
        df = pd.DataFrame({
            "id": [self.id],
            "theme": ["'"+self.theme+"'"],
            "description": ["'"+self.description+"'"],
            "date":[self.date]}
        )
        df = df.set_index("id")
        return df

    def next_id(self, datas):
        if(datas.shape[0] > 0):
            print(datas.columns)
            return datas.index[-1]+1
        else:
            return 1

    def ajout(self, datas):
        #ajouter
        id = self.next_id(datas)
        self.id = id
        return datas.append(self.act_object_to_data_frame(), sort = False)

    def modifier(self, datas):
        #modifier
        datas.loc[self.id] = self.actObjectToDataFrame()
        return datas

    def tri(self, datas, theme = ""):
        #tri
        if (theme == ""):
            return datas
        else:
            retour = datas
            if(theme != ""):
                retour = retour[retour["theme"] == theme]
        return retour

    def suppression(self, datas):
        #suppression
        return datas.drop(index = [self.id])

"""
act1 = Act(1, 'Sys', 'Install')
act2 = Act(1, 'Sys', 'Config')
datas = act1.actObjectToDataFrame()
datas = act2.ajout(datas)
print(datas)
actVaovao = Act(2, 'PythonDev', 'Install')
datas = actVaovao.modifier(datas)
print(datas)
datas = actVaovao.suppression(datas)
print(datas)
print(act1.nextId(datas))
print(datas.info())
print(actVaovao.tri(datas, "Sys"))

"""
