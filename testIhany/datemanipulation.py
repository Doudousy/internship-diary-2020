import pandas as pd
from pandas.tseries.offsets import DateOffset

ts = pd.Timestamp('2017-01-01 09:10:11')

ts1=ts + DateOffset(months=3)
ts1=ts + DateOffset(months=3,days=1)

ts = pd.Timestamp('2017-01-01 09:10:11')
ts2=ts + DateOffset(years=1,months=2,days=3,hours=4,minutes=5.5,seconds=6.006,microseconds=7,nanoseconds=8)

print(ts)
print(ts1)
print(ts2)