import pandas as pd
import testdatatypes



titanic = pd.read_csv("testIhany/datas/titanic.csv")
#titanic = pd.read_excel('testIhany/datas/titanic.xlsx', sheet_name='passengers')
print("")
print("reading data:")
#testdatatypes.display(titanic)
testdatatypes.display(titanic.head(8))
testdatatypes.display(titanic.info())
#print("")
#print("Export-data:")
#titanic.to_excel('testIhany/datas/titanic.xlsx', sheet_name='passengers', index=False)

print("select where equivalent")
above_35 = titanic[titanic["Age"] > 35]
testdatatypes.display(above_35.head())

class_23 = titanic[(titanic["Pclass"] == 2) | (titanic["Pclass"] == 3)]
testdatatypes.display(class_23.head())

adult_names = titanic.loc[titanic["Age"] > 35, "Name"]
testdatatypes.display(adult_names.head())

#I’m interested in rows 10 till 25 and columns 3 to 5.
testdatatypes.display(titanic.iloc[9:25, 2:5])

journal=pd.read_csv("testIhany/datas/diary.csv")
testdatatypes.display(journal)
