import pandas as pd
import numpy as np

import testdatatypes

# UNION
df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                    'B': ['B0', 'B1', 'B2', 'B3'],
                    'C': ['C0', 'C1', 'C2', 'C3'],
                    'D': ['D0', 'D1', 'D2', 'D3']},
                    index = [0, 1, 2, 3])


df2 = pd.DataFrame({'A': ['A4', 'A5', 'A6', 'A7'],
                    'B': ['B4', 'B5', 'B6', 'B7'],
                    'C': ['C4', 'C5', 'C6', 'C7'],
                    'D': ['D4', 'D5', 'D6', 'D7']},
                    index = [4, 5, 6, 7])


df3 = pd.DataFrame({'A': ['A8', 'A9', 'A10', 'A11'],
                    'B': ['B8', 'B9', 'B10', 'B11'],
                    'C': ['C8', 'C9', 'C10', 'C11'],
                    'D': ['D8', 'D9', 'D10', 'D11']},
                    index = [8, 9, 10, 11])


frames = [df1, df2, df3]
result = pd.concat(frames)
# testdatatypes.display(result)


left = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3'],
                    'A': ['A0', 'A1', 'A2', 'A3'],
                    'B': ['B0', 'B1', 'B2', 'B3']})


right = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3'], 
                        'C': ['C0', 'C1', 'C2', 'C3'], 
                        'D': ['D0', 'D1', 'D2', 'D3']}) 

# JOINTURE
result = pd.merge(left, right, on = 'key')
print("Jointure 1 column")
# testdatatypes.display(left)
# testdatatypes.display(right)
# testdatatypes.display(result)

left = pd.DataFrame({'key1': ['K0', 'K0', 'K1', 'K2'], 
                       'key2': ['K0', 'K1', 'K0', 'K1'], 
                       'A': ['A0', 'A1', 'A2', 'A3'], 
                         'B': ['B0', 'B1', 'B2', 'B3']})

right = pd.DataFrame({'key1': ['K0', 'K1', 'K1', 'K2'], 
                  'key2': ['K0', 'K0', 'K0', 'K0'], 
                    'C': ['C0', 'C1', 'C2', 'C3'], 
                    'D': ['D0', 'D1', 'D2', 'D3']})

result = pd.merge(left, right, on = ['key1', 'key2'])
print("Jointure plusieurs column")
testdatatypes.display(left)
testdatatypes.display(right)
testdatatypes.display(result)










df = pd.DataFrame([('bird', 'Falconiformes', 389.0), 
                     ('bird', 'Psittaciformes', 24.0), 
                     ('mammal', 'Carnivora', 80.2), 
                      ('mammal', 'Primates', np.nan), 
                      ('mammal', 'Carnivora', 58)], 
                     index = ['falcon', 'parrot', 'lion', 'monkey', 'leopard'], 
                     columns = ('class', 'order', 'max_speed'))
print(df)
def is_felin(order):
  if order != 'Carnivora':
    return 'not felin'
  else:
    return 'felin'

df['isFelin'] = df['order'].apply(isFelin)
# grouped = df.groupby('class')
# grouped = df.groupby(['class', 'order'])
grouped = df.groupby('isFelin')
print(grouped.size())
print(grouped.count())



dataflair_df1 = pd.DataFrame(6*np.random.randn(6, 3), columns = ['c1', 'c2', 'c3'])
print(dataflair_df1)
def adder(ele1, ele2):
  return ele1+ele2
print(dataflair_df1.pipe(adder, 3))
dfr = pd.DataFrame({
            "col1": ["a", "a", "b", "b", "a"], 
            "col2": [1.0, 2.0, 3.0, np.nan, 5.0], 
            "col3": [1.0, 2.0, 3.0, 4.0, 5.0]
        }, 
        columns = ["col1", "col2", "col3"], 
    )
df2 = dfr.copy()
df2.loc[0, 'col1'] = 'c'
df2.loc[2, 'col3'] = 4.0
# print(dfr)
# print(df2)
# print(dfr==(df2))

# MULTI INDEX
coords = [('AA', 'one'), ('AA', 'six'), ('BB', 'one'), ('BB', 'two'), 
             ('BB', 'six')]
index = pd.MultiIndex.from_tuples(coords)
df = pd.DataFrame([11, 22, 33, 44, 55], index, ['MyData'])
print(df.loc['AA'])
print(df.loc['AA'].loc['one'])