# DataFrames-and-Series
import pandas as pd

def display(data, description = ""):
    print(" --  -- ")
    print(description)
    print(data)

df = pd.DataFrame({
        "Name": ["Braund, Mr. Owen Harris",
                "Allen, Mr. William Henry",
                "Bonnell, Miss. Elizabeth"],
        "Age": [22, 35, 58],
        "Sex": ["male", "male", "female"]}
    )
display(df, "Dataframes-display:")

ages = df["Age"]
display(ages, "Series-display:")

ages = pd.Series([22, 35, 58], name = "Age")
display(ages, "Ages-display:")

# Statistics with datas
display(ages.max(), "Age max")

display(df.describe(), "df-Description:")
