import datetime
import pandas as pd
import numpy as np
from openpyxl.utils import get_column_letter

dti = pd.to_datetime(['1/1/2018', np.datetime64('2018-01-01'), datetime.datetime(2018, 1, 1)])
print(dti)
dti = dti.tz_localize('UTC')
# print(dti)
d = {'a': 0., 'b': 1., 'c': 2.}
# print(d)
# print(type(d))
b = pd.Series(5., index = ['a', 'b', 'c', 'd', 'e'])
# print(b)

# Combining overlapping data sets
df1 = pd.DataFrame({'A': [1., np.nan, 3., 5., np.nan], 'B': [np.nan, 2., 3., np.nan, 6.]})
# print(df1)
df2 = pd.DataFrame({'A': [5., 2., 4., np.nan, 3., 7.], 'B': [np.nan, np.nan, 3., 4., 6., 8.]})
# print(df2)
df3 = (df1.combine_first(df2))
print(df3)
# moyenne des lignes par colonne (axis=0)
# print(df3.mean(0))
# moyenne des colonnes par ligne (axis=1)
# print(df3.mean(1))
print(df3.loc[1])
print(df3.pow(2))

def export_html():
    test = pd.read_csv('testIhany/datas/diary2.csv', sep = ';')
    test = test.set_index("id")
    print(test)
    test["description"] = test["description"].str.replace('\t', ' <br> ')
    print(test)
    mystyle = test.style.set_caption('Journal de stage')
    print(mystyle.export())
    f = open('testIhany/datas/diary2.html', 'w')
    html_string = '''
        <html>
        <head>  <title> HTML Pandas Dataframe with CSS </title>  </head>
        <!-- CSS -->
        <link rel = "stylesheet" href = "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity = "sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin = "anonymous" >

        <!-- jQuery and JS bundle w/ Popper.js -->
        <script src = "https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity = "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin = "anonymous" >  </script>
        <script src = "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity = "sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin = "anonymous" >  </script>
        <link rel = "stylesheet" type = "text/css" href = "style.css"/ >
        <body >
            {table}
        </body>
        </html> .
        '''
    f.write(html_string.format(table = test.to_html(justify = 'center', classes = 'table table-hover table-borderless', escape = False)))
    f.close()

export_html()

def export_excel():
    test = pd.read_csv('testIhany/datas/diary2.csv', sep = ';')
    test = test.set_index("id")
    print(test)
    test.to_excel('testIhany/datas/diary2.xlsx', index_label = 'id', merge_cells = False, sheet_name = 'journal')
    xlsFilepath = 'testIhany/datas/diary2.xlsx'
    writer = pd.ExcelWriter(xlsFilepath, engine = 'openpyxl')
    #Write excel to file using pandas to_excel
    test.to_excel(writer, startrow = 1, merge_cells = False, sheet_name = 'journal', index_label = 'id')

    #Indicate workbook and worksheet for formatting
    workbook = writer.book
    worksheet = writer.sheets['journal']
    worksheet
    dims = {}
    for row in worksheet.rows:
        for cell in row:
            if cell.value:
                dims[cell.column] = 1 + max((dims.get(cell.column, 0), len(str(cell.value))))
    i = 0
    for col, value in dims.items():
        worksheet.column_dimensions[get_column_letter(i+1)].width = value
        i = i+1
    writer.save()

export_excel()